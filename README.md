# Workshop Robot Framework with Browser Library

## Preconditions
- Have nodeJS installed: https://nodejs.org/en/download/
- Have Python installed: https://www.python.org/downloads/
    - During installation enable option add to path
    - During installation make sure pip is installed as well: should be done automatically. System restart might be needed.
    - If Python was already installed you can update with `pip install -U pip` to ensure latest version is used
- Have vscode installed: https://code.visualstudio.com/download
- Have either Chrome or Firefox browser installed

## 1. Installation

### Create our first Robot Framework project

1. Create an empty folder to start your project in, give it a name and open this folder in vscode
2. Use ctrl+` in vscode to start the integrated terminal (or select terminal from the view menu)
3. Install Robot Framework: `pip install robotframework`
    - For updating Robot Framework you can use: `pip install --upgrade robotframework`
4. Install robotframework-browser: `pip install robotframework-browser`
    - To check if it was installed use: `pip list`, you should see robotframework and robotframework-browser in the list of python packages
    - You can also try to run the `robot` command, you should see an error stating there were no arguments given
5. Install the node dependencies: `rfbrowser init`
    - if rfbrowser is not found, try `python -m Browser.entry init`

## 2. Writing and running tests: part 1 - The Starting

### Create our first Robot Framework file

1. Create a folder called 'Tests' in your project root
2. In the 'Tests' folder create another folder called 'Example_Tests'
3. In the 'Example Tests' folder create a file called 'first_test.robot'
4. Add the following snippet to this file to create our first test case:

  ```
*** Settings ***
Documentation      This is my first test case
...                and I hope it works well!
Library            Browser

*** Test Cases ***
Example Test
    Open Browser   https://testshop.polteq-testing.com/
    Get Text       id=contact-link    contains    Contact us
  ```
5. Open Extensions Sidebar (Ctrl + Shift + X)
6. Search for `open in browser` and install the first option (This way we can easily open the html files via vscode in our browser)
7. Search for `Robot Framework Language Server` and install it

### Try to run this first test with this command:
1. `robot Tests` ('Tests' is the name of the first folder you created, if you gave it another name then you must use that one. You can also use path to the sub folder or the filename)
2. Open the 'report.html' file to see the results in detail. You can find it in the root of your project folder. Open by right click and click 'Open In Default Browser'.
3. Check out the report by clicking around. You can see the report is green when all tests pass, it will be red if one test fails. There are search and filter options which might come in handy when you have a lot of tests/suites. You can click on a test case to open the log and see more detail of each keyword.

### Robot Framework syntax basics

- A Robot Framework file (*.robot) usually consists of a few sections
- For example the *** Settings *** and *** Test Cases *** are recognized as different sections (we will add two more after this)
- Robot Framework interprets table structured text files, so spacing is important 
- A test case starts at the beginning of the line and all the steps in it should be indented (same goes for keywords in the Keywords section)
- Every keyword or argument should be separated by at least two spaces for Robot Framework to be able to recognize it as two different things
- Variables look like this: ${variable_name}, good practice is to use lower case for local variables and upper case for global variables (at least be consequent)
- The tests in the test cases section can have additional settings like [Documentation], [Tags], [Setup] and [Teardown]

### Add Variables and Keywords section
1. Add the following snippet to the test file between the Settings and Test Cases section
```
*** Variables ***

*** Keywords ***
```
2. In the Variables section add a variable for the url and add another variable for the contact link element
3. In the Keywords section add a keyword that navigates to the homepage
4. Use the variables in the test and keyword

### Rerun the test to check if it still works with command:
- `robot Tests`

## 3. Writing and running tests: part 2 - The Structuring

### Implement init, resources and keywords files
1. Create a file called '\_\_init\_\_.robot' in your 'Tests' folder (so with two underscores before and after the word init). The init file always runs first when running your Tests folder
2. Add the following snippet to the init file: 

```
*** Settings ***
Resource           ./workshop.resource
Suite Setup        Start new Browser
Suite Teardown     Close Browser
Test Setup         Run Keywords  New Context  AND  New Page

*** Keywords ***
Start new Browser
    New Browser  ${BROWSER}  headless=False
```
3. Create a file called 'workshop.resource' in your 'Tests' folder 
4. Add the following snippet to the resource file:
```
*** Settings ***
Library            Browser

*** Variables ***
${BROWSER}=        chromium  #firefox
${URL}=            https://testshop.polteq-testing.com/
```
5. Create a file called 'keywords.robot' in your 'Tests' folder
6. Add the following snippet to your keywords file:
```
*** Settings ***
Resource        workshop.resource

*** Variables ***


*** Keywords ***
```

### Refactor your first_test file
1. Add this to your 'first_test' file in the Settings section:
```
Resource           ./../workshop.resource
```
2. Remove the Browser library import from your Settings section
3. Remove url variable from Variables section
4. Change the \${url} variable in the Keywords section to ${URL} (although it still is recognized!)

### Rerun the test to check if it still works with command:
- `robot Tests`
- We will use the created keywords file next

## 4. Writing and running tests: part 3 - The Data Driving: Test Template

### Fill the keywords file and add invalid login tests
1. Create a folder called 'Login_Tests' in your 'Tests' folder
2. Create a file called 'invalid_login_tests.robot' in the 'Login_Tests' folder
3. Add the following snippet to this file:
```
*** Settings ***
Resource        ../keywords.robot
Resource        ../workshop.resource
Test Template   Login with invalid credentials should give correct error message
Force Tags      datadriven  # This tag is added to all test cases in this test suite

*** Variables ***
${valid_username}=        # created user
${valid_password}=        # created password

*** Keywords ***
Login with invalid credentials should give correct error message
    [Arguments]    ${username}    ${password}
    Open homepage
    Navigate to the login page
    Login                        ${username}          ${password}
    Login Error Should Be Shown

*** Test Cases ***                USERNAME             PASSWORD
Invalid Username                  invalid@test.nl      ${valid_password}
    [Tags]  firstdd  # This tag only applies to this test case, in addition to the datadriven tag
Invalid Password                  ${valid_username}    invalid
Invalid Username and Password     invalid@test.nl      invalid
```
4. Add the following snippet to the keywords file:
```
*** Variables ***
${authenticationLink}=  [title='Log in to your customer account']
${emailTextField}=      //*[@id='login-form']//*[@type='email']  
${passwordTextfield}=   //*[@name='password']
${loginButton}=         id=submit-login
${logInError}=          text="Authentication failed."

*** Keywords ***   
Open homepage
    Go To        ${URL}

Navigate to the login page
    Click    ${authenticationLink}

Login
    [Arguments]      ${emailAddress}       ${password}
    Fill Text        ${emailTextField}     ${emailAddress}
    Fill Text        ${passwordTextfield}  ${password}
    Click            ${loginButton}

Login Error Should Be Shown 
    Get Text  ${logInError}  Contains  Authentication failed
```
4. Create a test user on the Polteq testshop and add the username/email address and password to the Variables section
5. Run these tests using the following command:
- `robot --include datadriven Tests`

## 5. Writing and running tests: part 4 - The Gherkining

### Let's add one more test gherkin style
1. Create a new folder called 'Shopping_Cart_Tests' in your 'Tests' folder
2. Create a new file called 'add_to_cart_test.robot' in this folder
3. Add the following snippet to this file:
```
*** Settings ***
Resource   ./../workshop.resource

*** Variables ***
${searchField} =            [name='s']
${searchButton} =           \#search_widget button
${searchResult} =           \#js-product-list [itemprop='itemListElement']:nth-child(2)
${addToCartButton} =        [data-button-action='add-to-cart']
${cartModalHeaderText} =    \#blockcart-modal .modal-header

*** Keywords ***
I open the homepage
    Go To    ${URL}

I search for ${searchTerm}
    Type Text     ${searchField}    ${searchTerm}
    Click         ${searchButton}
    Click         ${searchResult}

I click on the add to cart button
    Click         ${addToCartButton}

The product is successfully added to my shopping cart
    Wait For Condition  Text      ${cartModalHeaderText}      contains      Product successfully added to your shopping cart

*** Test Cases ***
Add to cart test
    [Tags]    gherkin
    Given I open the homepage
    And I search for mug
    When I click on the add to cart button
    Then The product is successfully added to my shopping cart
```
4. Run this test with command:
`robot --include gherkin Tests`

## 6. Writing and running tests: part 5 - The Trying yourself

### See if you can add tests yourself or play around with the existing ones
1. Suggestion one: add a valid login test (create a new file)
2. Suggestion two: add a data driven test using a test template (create a new file)
3. Suggestion three: add a gherkin style test case (create a new file or change an existing test)
4. Suggestion four: refactor the test cases: move keywords and variables to the keywords or resources file
5. Suggestion five: add a create an account test

### Checkout the documentation:
1. For Robot framework: https://robotframework.org/  and  https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#getting-started
2. For Browser Library: https://robotframework-browser.org/  and  https://marketsquare.github.io/robotframework-browser/Browser.html

Thanks for playing!