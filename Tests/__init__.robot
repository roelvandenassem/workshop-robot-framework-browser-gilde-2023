*** Settings ***
Resource           workshop.resource
Suite Setup        Start new Browser
Suite Teardown     Close Browser
Test Setup         Run Keywords  New Context  AND  New Page

*** Keywords ***
Start new Browser
    New Browser  ${BROWSER}  headless=False
    # Set Strict Mode    False
    # Set Browser Timeout    15
