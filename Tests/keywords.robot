*** Settings ***
Resource        workshop.resource

*** Variables ***
${authenticationLink}=  [title='Log in to your customer account']
${emailTextField}=      //*[@id='login-form']//*[@type='email']  
${passwordTextfield}=   //*[@name='password']
${loginButton}=         id=submit-login
${logoutButton}=        .logout
${accountButton}=       .account
${pageHeader}=          .page-header
${logInError}=          text="Authentication failed."

*** Keywords ***   
Open homepage
    Go To        ${URL}

Navigate to the login page
    Click    ${authenticationLink}

Login
    [Arguments]      ${emailAddress}       ${password}
    # Alternatively you can use the keyword Type Text to emulate typing
    Fill Text        ${emailTextField}     ${emailAddress}
    Fill Text        ${passwordTextfield}  ${password}
    Click            ${loginButton}

User Should Be Logged In 
     Get Text  ${pageHeader}  ==  Your account
     
Login Error Should Be Shown 
    Get Text  ${logInError}  Contains  Authentication failed

# IsLoggedIn
#     ${headerText}=  Get Text  ${pageHeader}
#     IF    "${headerText}" == "Your account"  
#         RETURN  ${True}
#     ELSE
#         RETURN  ${False}
#     END

    # ${logOut}=  Get Elements  ${logoutButton}
    # ${logOut}=  Get Length    ${logOut}
    # IF    ${logOut} > 0  
    #     RETURN  ${True}
    # ELSE
    #     RETURN  ${False}
    # END

# LogInErrorIsShown
#     ${errorText}=  
#     IF    "${errorText}" == "Authentication failed."  
#         RETURN  ${True}
#     ELSE
#         RETURN  ${False}
#     END

    # ${error}=  Get Elements  ${logInError}
    # ${error}=  Get Length    ${error}
    # IF    ${error} > 0  
    #     RETURN  ${True}
    # ELSE
    #     RETURN  ${False}
    # END
