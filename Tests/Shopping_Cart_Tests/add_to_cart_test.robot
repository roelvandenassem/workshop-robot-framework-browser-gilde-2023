*** Settings ***
Library    Browser
Resource   ./../workshop.resource
Resource   ./../keywords.robot

*** Variables ***
${url} =                    https://testshop.polteq-testing.com/
${searchField} =            [name='s']
${searchButton} =           \#search_widget button
${searchResult} =           \#js-product-list [itemprop='itemListElement']:nth-child(2)
${addToCartButton} =        [data-button-action='add-to-cart']
${cartModalHeaderText} =    \#blockcart-modal .modal-header

*** Keywords ***
Search For
    [Arguments]   ${searchTerm}
    Type Text     ${searchField}    ${searchTerm}
    Click         ${searchButton}
    Click         ${searchResult}

*** Test Cases ***
Add to cart test
    [Tags]    atc
    Open homepage
    Search For     mug
    Click         ${addToCartButton}
    Get Text      ${cartModalHeaderText}      contains      Product successfully added to your shopping cart
