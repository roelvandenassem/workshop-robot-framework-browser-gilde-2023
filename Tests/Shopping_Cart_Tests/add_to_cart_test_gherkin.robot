*** Settings ***
Resource   ./../workshop.resource

*** Variables ***
${searchField} =            [name='s']
${searchButton} =           \#search_widget button
${searchResult} =           \#js-product-list [itemprop='itemListElement']:nth-child(2)
${addToCartButton} =        [data-button-action='add-to-cart']
${cartModalHeaderText} =    \#blockcart-modal .modal-header

*** Keywords ***
I open the homepage
    Go To    ${URL}

I search for ${searchTerm}
    Type Text     ${searchField}    ${searchTerm}
    Click         ${searchButton}
    Click         ${searchResult}

I click on the add to cart button
    Click         ${addToCartButton}

The product is successfully added to my shopping cart
    Wait For Condition  Text      ${cartModalHeaderText}      contains      Product successfully added to your shopping cart

*** Test Cases ***
Add to cart test
    [Tags]    gherkin
    Given I open the homepage
    And I search for mug
    When I click on the add to cart button
    Then The product is successfully added to my shopping cart
