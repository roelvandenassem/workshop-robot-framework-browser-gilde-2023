*** Settings ***
Documentation      This is my first test case
...                and I hope it works well!
Resource           ./../workshop.resource  # You can parameterize the path, f.e: ${RESOURCES}/workshop.resource

*** Variables ***
# This is a local variable
${contactLink}=    id=contact-link

*** Keywords ***
Open the homepage
    Go To   ${URL}  # This is a global variable

*** Test Cases ***
Example Test
    [Tags]  smoke  # You can add as many tags as you want
    Open the homepage
    Get Text        ${contactLink}    Contains    Contact us

*** Comments ***
All this text will be ignored during test execution.
But you can add comments if needed.

Other options for the locator in the Variables section are:
css=id=contact-link
\#contact-link
xpath=//*[@id='contact-link']
//*[@id='contact-link']
"Contact us"
text="Contact us"
