*** Settings ***
Resource           ./../workshop.resource 
Test Setup         Run Keywords  Open Browser  AND  Open the homepage

*** Variables ***
${authenticationLink}=  [title='Log in to your customer account']
${registrationLink}=    [data-link-action='display-register-form']
${privacyCheckbox}=     [name='customer_privacy']
${emailField}=          [class='form-control'][name='email']


*** Keywords ***
Open the homepage
    Go To   ${URL}

*** Test Cases ***
Checkbox Example
    [Tags]  extra
    Click               ${authenticationLink}
    Click               ${registrationLink}
    Check Checkbox      ${privacyCheckbox}
    Sleep               2
    Uncheck Checkbox    ${privacyCheckbox}
    Sleep               2

Clear Text Example
    [Tags]  extra
    Click            ${authenticationLink}
    Click            ${registrationLink}
    Fill Text        ${emailField}            txt
    Sleep            2
    Clear Text       ${emailField}
    Sleep            2
