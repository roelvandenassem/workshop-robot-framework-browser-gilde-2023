*** Settings ***
Resource        ../keywords.robot
Resource        ../workshop.resource
Test Template   Login with invalid credentials should give correct error message
Force Tags      datadriven  # This tag is added to all test cases in this test suite

*** Variables ***
${valid_username}=        roel@test.com
${valid_password}=        1qazxsw2

*** Keywords ***
Login with invalid credentials should give correct error message
    [Arguments]    ${username}    ${password}
    Open homepage
    Navigate to the login page
    Login                        ${username}          ${password}
    Login Error Should Be Shown

*** Test Cases ***                USERNAME             PASSWORD
Invalid Username                  invalid@test.nl      ${valid_password}
    [Tags]  firstdd  # This tag only applies to this test case, in addition to the datadriven tag
Invalid Password                  ${valid_username}    invalid
Invalid Username and Password     invalid@test.nl      invalid

# Empty Username        ${EMPTY}             ${valid_password}
# Empty Password        ${valid_username}    ${EMPTY}
# Empty Both            ${EMPTY}             ${EMPTY}


